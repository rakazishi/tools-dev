# Tools

## Web

* [Insomnia REST](https://insomnia.rest/) - REST GUI Client
* https://github.com/denji/awesome-http-benchmark
* https://github.com/caronc/apprise

## Services

* https://www.hetzner.com/cloud - Cheap cloud servers
* https://mikr.us/ - Small & cheap VPS

## Monitoring

* https://github.com/netdata/netdata 

## Command Line

* [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) - zsh configuration management tool
* [fzf](https://github.com/junegunn/fzf) - A command-line fuzzy finder
* [ag](https://github.com/ggreer/the_silver_searcher) - A code-searching tool similar to ack, but faster.
* [tldr](https://tldr.sh/) - Simplified and community-driven man pages
* [sshuttle](https://github.com/sshuttle/sshuttle) - Transparent proxy server that works as a poor man's VPN. Forwards over ssh. Doesn't require admin.
* [nvm](https://github.com/nvm-sh/nvm) - Node version manager script

### Elasticsearch

* [elasticdump](https://www.npmjs.com/package/elasticdump) - Elasticsearch tools for moving and saving indices.

### Linux

* [Guake](https://github.com/Guake/guake) - Drop down terminal for Gnome
* [Solaar](https://pwr-solaar.github.io/Solaar/) - GUI for managing Logitech Unifying devices

### macOS
* [iTerm2](https://iterm2.com) - Drop down terminal for macOS


# Resources

## Aggregated

* [Front-End Developer Handbook 2018](https://frontendmasters.com/books/front-end-handbook/2018/)
* https://github.com/trimstray/nginx-quick-reference
* https://github.com/dylanaraps/pure-bash-bible

## Courses

* https://egghead.io

## Blogs

* https://overreacted.io/

## Security

* https://www.owasp.org/index.php/Testing_Checklist


## Java 

* https://advancedweb.hu/a-categorized-list-of-all-java-and-jvm-features-since-jdk-8-to-16/

